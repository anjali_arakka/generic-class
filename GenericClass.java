public class GenericClass<T> {


    T value;


    public void setValue(T value) {
        this.value = value ;
    }



    public T getValue() {
        return value ;

    }

    public static <T> void print(T anyValue) {
        System.out.println(anyValue);
    }




    public static void main(String[] args) {


        GenericClass<Integer> object1 = new GenericClass<>();
        object1.setValue(22) ;
        System.out.println(object1.getValue());


        String string = "This is generic method" ;
        int integer = 359 ;
        boolean bool = true ;

        print(string);
        print(integer);
        print(bool);





    }
}
